import csv 
import argparse
import requests
from random import randint
from time import sleep
from datetime import datetime
import os.path


OVERRIDEDELAY = False
TESTMODE = False
BASEURL = 'https://ssl.gstatic.com/dictionary/static/sounds/de/0/{0}.mp3'

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile")
    parser.add_argument("outputdir")
    args = parser.parse_args()
    return args

def parsefile(fpath):
    lstWords = []
    try:
        with open(fpath, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                lstWords.append(row[0])
    except FileNotFoundError:
        print("Sorry the file '{}' does not exist".format(fpath))

    return lstWords

def makeoutputpaths(w, outputdir):
    mp3fileout = "./{}/{}.mp3".format(outputdir, w)
    logfileout = "./{}/{}.log".format(outputdir, w)

    return {'mp3path': mp3fileout, 'logfileout': logfileout}

def alreadyprocessed(lstfilenames):
    for p in lstfilenames:
        if (os.path.isfile(p)):
            return True

    return False

def fetchfiles(lstwords, outputdir):
    wcnt = 0
    wgood = 0
    wbad = 0

    if TESTMODE:
        lstwords = ['ZZZ', 'ttt', 'xxx']

    for w in lstwords:
        d=datetime.now()
        isonow = d.strftime('%Y%m%d %H%M%S')
        w = w.replace("'", "").strip()
        w = w.lower()
        
        dicpaths = makeoutputpaths(w, outputdir)
        import pprint
        pprint.pprint(dicpaths)

        if alreadyprocessed([dicpaths['mp3path'], dicpaths['logfileout']]):
            print("[{}] Skipping {} due to existence of either {} or {}. {} good. {} bad.".format(isonow, w, dicpaths['mp3path'], dicpaths['logfileout'], wgood, wbad))
            wcnt += 1
            wgood += 1
        else:
            if (TESTMODE == False) and (OVERRIDEDELAY == False):
                sleep(randint(10,100))
            url = BASEURL.format(w)
            r = requests.get(BASEURL.format(w))
            if r.status_code == requests.codes.ok:
                fileout = open(dicpaths['mp3path'],"wb")
                fileout.write(bytes(r.content))
                fileout.close()
                wgood += 1
            else:
                fileout = open(dicpaths['logfileout'],"w")
                print("{} - Unable to find sound file for word '{}' at : {}".format(isonow, w, url), file=fileout)
                fileout.close()
                wbad += 1
            wcnt += 1
            if (wcnt < 5) or (wcnt % 10 == 0):
                print("[{}] {} of {} files processed. {} good. {} bad.".format(isonow, wcnt, len(lstwords), wgood, wbad))

            if TESTMODE:
                if (wcnt > 1):
                    raise NotImplementedError


def main():
    args = get_args()
    data = parsefile(args.inputfile)
    fetchfiles(data, args.outputdir)

if __name__ == "__main__":
    main()
