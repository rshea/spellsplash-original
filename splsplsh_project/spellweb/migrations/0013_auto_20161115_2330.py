# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-15 10:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spellweb', '0012_word_soundsource'),
    ]

    operations = [
        migrations.AlterField(
            model_name='word',
            name='hint',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
