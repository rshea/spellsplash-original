from django.conf.urls import patterns, url
from django.contrib import admin
admin.autodiscover()
admin.site.site_header = 'SpellSplash Administration Area'
admin.site.site_title = 'SpellSplash Admin'

from spellweb import views

urlpatterns = [ 
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'lc/', views.LearnerCreate.as_view(), name='anamedurl'),
    url(r'attempt/', views.attempt_create, name='attcrea'),
    url(r'attemptonebyoneHIDE/', views.attempt_onebyone_createHIDE, name='attemptonebyoneHIDE'),
    url(r'attemptonebyone/', views.WordQuestionListView.as_view(), name='attemptonebyone'),
    url(r'attemptsubmission/', views.attempt_submission, name='attemptsubmission'),
    url(r'showresults/', views.AttemptListView.as_view(), name='displayresults'),
    url(r'testaudio/', views.testaudio, name='testaudio'),
    url(r'testaudiodynamic/', views.WordListView.as_view(), name='testaudiodynamic'),
]
