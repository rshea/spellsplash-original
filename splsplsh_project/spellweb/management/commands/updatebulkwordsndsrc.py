from django.core.management.base import BaseCommand
from django.db import transaction
from spellweb.models import Word, Learner, Attempt, Box
from django.core.exceptions import ObjectDoesNotExist
from .bulkdatainput import BASEURL, LSTWORDS

class Command(BaseCommand):
    help = 'Provides a bulk update of Word objects to provide the URLs where the sounds can be obtained'

    def handle(self, *args, **options):
        for refstring in LSTWORDS:
            try:
                objW = Word.objects.get(word=refstring)
                objW.soundsource = BASEURL.format(refstring) 
                objW.save()
                self.stdout.write('Updated {}'.format(objW.word))
            except ObjectDoesNotExist:
                self.stdout.write('Failed to find {}'.format(objW.word))


