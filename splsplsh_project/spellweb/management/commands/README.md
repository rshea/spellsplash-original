Each time the script to do a bulk upload is run the relevant
data should be entered into bulkdatainput.py and then the 
the script bulkinput.py will be run.

The script is run from the Django shell as follows :

 ./manage.py shell < myscript.py

